export default function totalSetCount(state = 0, action) {
  switch (action.type) {
    case "INCREMENT_TOTALSETCOUNT":
      return state + 1;
    case "CHANGE_TOTALSETCOUNT":
      return action.payload;
    default:
      return state;
  }
}
