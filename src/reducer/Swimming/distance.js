export default function distance(state = 25, action) {
  switch (action.type) {
    case "CHANGE_DISTANCE":
      return action.payload;
    default:
      return state;
  }
}
