export default function drill(state = "", action) {
  switch (action.type) {
    case "CHANGE_DRILL":
      return action.payload;
    default:
      return state;
  }
}
