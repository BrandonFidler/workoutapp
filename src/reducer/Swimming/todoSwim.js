export default function todoSwim(state = [], action) {
  switch (action.type) {
    case "CHANGE_TODOSWIM":
      return action.payload;
    default:
      return state;
  }
}
