export default function repeat(state = 1, action) {
  switch (action.type) {
    case "CHANGE_REPEAT":
      return action.payload;
    default:
      return state;
  }
}
