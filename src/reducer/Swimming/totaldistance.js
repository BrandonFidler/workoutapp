export default function totaldistance(state = null, action) {
  switch (action.type) {
    case "CHANGE_TOTALDISTANCE":
      return action.payload;
    default:
      return state;
  }
}
