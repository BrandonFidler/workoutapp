export default function swimCount(state = 0, action) {
  switch (action.type) {
    case "INCREMENT_SWIMCOUNT":
      return state + 1;
    case "CHANGE_SWIMCOUNT":
      return action.payload;
    default:
      return state;
  }
}
