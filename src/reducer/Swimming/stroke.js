export default function stroke(state = "Butterfly", action) {
  switch (action.type) {
    case "CHANGE_STROKE":
      return action.payload;
    default:
      return state;
  }
}
