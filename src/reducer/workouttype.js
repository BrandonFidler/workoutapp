export default function workouttype(state = "Swimming", action) {
  switch (action.type) {
    case "CHANGE_WORKOUTTYPE":
      return action.payload;
    default:
      return state;
  }
}
