export default function viewCompletedSet(state = "", action) {
  switch (action.type) {
    case "CHANGE_VIEWCOMPLETEDSET":
      return action.payload;
    default:
      return state;
  }
}
