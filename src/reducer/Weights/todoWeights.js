export default function todoWeights(state = [], action) {
  switch (action.type) {
    case "CHANGE_TODOWEIGHTS":
      return action.payload;
    default:
      return state;
  }
}
