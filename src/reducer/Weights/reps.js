export default function reps(state = 1, action) {
  switch (action.type) {
    case "CHANGE_REPS":
      return action.payload;
    default:
      return state;
  }
}
