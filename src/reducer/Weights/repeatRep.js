export default function repeatRep(state = 1, action) {
  switch (action.type) {
    case "CHANGE_REPEATREP":
      return action.payload;
    default:
      return state;
  }
}
