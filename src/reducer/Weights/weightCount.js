export default function weightCount(state = 0, action) {
  switch (action.type) {
    case "INCREMENT_WEIGHTCOUNT":
      return state + 1;
    case "CHANGE_WEIGHTCOUNT":
      return action.payload;
    default:
      return state;
  }
}
