export default function weight(state = "body", action) {
  switch (action.type) {
    case "CHANGE_WEIGHT":
      return action.payload;
    default:
      return state;
  }
}
