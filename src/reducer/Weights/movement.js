export default function movement(state = "Squats", action) {
  switch (action.type) {
    case "CHANGE_MOVEMENT":
      return action.payload;
    default:
      return state;
  }
}
