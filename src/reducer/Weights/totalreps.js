export default function totalreps(state = 0, action) {
  switch (action.type) {
    case "CHANGE_TOTALREPS":
      return action.payload;
    default:
      return state;
  }
}
