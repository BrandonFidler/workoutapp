export default function completedSets(state = [], action) {
  switch (action.type) {
    case "CHANGE_COMPLETEDSETS":
      return action.payload;
    default:
      return state;
  }
}
