import { combineReducers } from "redux";
import workouttype from "./workouttype.js";
import distance from "./Swimming/distance";
import totaldistance from "./Swimming/totaldistance";
import stroke from "./Swimming/stroke";
import todoSwim from "./Swimming/todoSwim";
import repeat from "./Swimming/repeat";
import drill from "./Swimming/drill";
import reps from "./Weights/reps";
import totalreps from "./Weights/totalreps";
import movement from "./Weights/movement";
import todoWeights from "./Weights/todoWeights";
import repeatRep from "./Weights/repeatRep";
import weight from "./Weights/weight.js";
import completedSets from "./completedSets.js";
import viewCompletedSet from "./viewCompletedSet.js";
import swimCount from "./Swimming/swimCount.js";
import weightCount from "./Weights/weightCount.js";
import totalSetCount from "./totalSetCount.js";

export default combineReducers({
  workouttype, //same as type:type
  //swimming
  distance,
  totaldistance,
  stroke,
  todoSwim,
  repeat,
  drill,
  swimCount,
  //weights
  reps,
  totalreps,
  movement,
  todoWeights,
  repeatRep,
  weight,
  weightCount,
  //Completed
  viewCompletedSet,
  totalSetCount,
  completedSets,
});
