import React from "react";
import { useState } from "react";
import { useDispatch } from "react-redux";
import Set from "./Set.jsx";
import incrementswimCount from "./actionCreators/Swimming/incrementswimCount.js";
import incrementweightCount from "./actionCreators/Weights/incrementweightCount.js";
import incrementtotalSetCount from "./actionCreators/incrementtotalSetCount.js";

//takes in props from SwimmingOptions, or WeightsOptions.
//starts to create the table for showing the sets created.
//creates an object for use in Completed Sets Section.
const Setlist = ({
  todo,
  changeTodo,
  total,
  changeTotal,
  workouttype,
  LABEL,
  swimCount,
  weightCount,
  totalSetCount,
  completedSets,
  changeCompletedSets,
}) => {
  const dispatch = useDispatch();
  var today = new Date();
  const [date, setDate] = useState(
    today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate()
  );
  const deleteAllHandler = () => {
    dispatch(changeTodo([]));
    dispatch(changeTotal(0));
  };

  const completeSet = (e) => {
    e.preventDefault();
    let setkey = "";
    let swimsetlabel = "";
    let setkey2 = "";
    today = new Date();
    setDate(
      today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate()
    );

    if (workouttype === "Swimming") {
      setkey = `${date} swim ${swimCount} ${Math.random() * 1000}`;
      swimsetlabel = `${date} swim ${swimCount}`;
      setkey2 = `swimset ${Math.random() * 1000}`;
      dispatch(incrementswimCount());
    } else {
      setkey = `${date} weight ${weightCount} ${Math.random() * 1000}`;
      swimsetlabel = `${date} weight ${weightCount}`;
      setkey2 = `weightset ${Math.random() * 1000}`;
      dispatch(incrementweightCount());
    }
    dispatch(incrementtotalSetCount());

    dispatch(
      changeCompletedSets([
        ...completedSets,
        {
          completed: todo,
          date: date,
          key: setkey,
          optionlabel: swimsetlabel,
          setkey2: setkey2,
          count: totalSetCount,
          LABEL: LABEL,
          workouttype: workouttype,
        },
      ])
    );
  };

  return (
    <div className="flex flex-col justify-center items-center">
      <div className="flex flex-row">
        <h2 className="flex justify-center font-bold">
          Total {LABEL.total} = {total} {LABEL.unit}
        </h2>
        <button className="selectionButton bg-green-500" onClick={completeSet}>
          Complete Set
        </button>
      </div>
      <ul className="flex flex-col border border-black">
        <li className="flex items-center flex-row hover:bg-gray-100 ">
          <span className="setlistheader">{LABEL.first}</span>
          <span className="setlistheader">{LABEL.second}</span>
          <span className="setlistheader">{LABEL.third}</span>
          <span className="setlistheader">{LABEL.fourth}</span>
          <button className="deleteButton" onClick={deleteAllHandler}>
            <i>Delete All</i>
          </button>
        </li>
        {todo.map((todoitem) => (
          <Set
            todo={todo}
            changeTodo={changeTodo}
            total={total}
            changeTotal={changeTotal}
            key={todoitem.id}
            todoitem={todoitem}
            workouttype={workouttype}
          />
        ))}
      </ul>
      <span className="p-10"></span>
    </div>
  );
};

export default Setlist;
