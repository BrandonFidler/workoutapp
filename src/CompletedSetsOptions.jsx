import React from "react";
import { useSelector, useDispatch } from "react-redux";
import changeviewCompletedSet from "./actionCreators/changeviewCompletedSet";
import CompletedList from "./CompletedList.jsx";

//component for the selection of completed sets
const CompletedSetsOptions = ({
  completedSets,
  SWIMMINGLABELS,
  WEIGHTSLABELS,
}) => {
  const dispatch = useDispatch();
  const viewCompletedSet = useSelector((state) => state.viewCompletedSet);

  function selectHandler(index) {
    console.log(index);
    dispatch(changeviewCompletedSet(completedSets[index]));
  }

  if (completedSets.length !== 0) {
    return (
      <div className="my-0 mx-auto w-8/12  2xl:w-4/12">
        <form
          onSubmit={(e) => {
            e.preventDefault();
          }}
          className="  p-10 mb-10 rounded-lg bg-gray-200 shadow-lg flex flex-col 2xl:flex-row  justify-center items-center "
        >
          <label htmlFor="completedSwimmingOptions">
            <i className="font-bold p-2 text-center">Completed Sets</i>

            <select
              name="completed"
              id="completed"
              onChange={(e) => selectHandler(e.target.value)}
              onBlur={(e) => selectHandler(e.target.value)}
              className=" mr-10"
            >
              <option />
              {completedSets.map((setitem) => (
                <option value={setitem.count} id="setitem" key={setitem.key}>
                  {setitem.optionlabel}
                </option>
              ))}
            </select>
          </label>
        </form>
        <CompletedList
          key={viewCompletedSet.date}
          todo={viewCompletedSet}
          SWIMMINGLABELS={SWIMMINGLABELS}
          WEIGHTSLABELS={WEIGHTSLABELS}
        />
      </div>
    );
  } else {
    return <div className="flex justify-center">
    <i className="font-bold p-2 text-center">No Completed Workouts</i>
  </div>;
  }
};

export default CompletedSetsOptions;
