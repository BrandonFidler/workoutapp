import React from "react";
import SwimmingOptions from "./SwimmingOptions.jsx";
import WeightsOptions from "./WeightsOptions.jsx";
import CompletedSetsOptions from "./CompletedSetsOptions.jsx";
import changeswimCount from "./actionCreators/Swimming/changeswimCount.js";
import changeTodoSwim from "./actionCreators/Swimming/changeTodoSwim";
import changeweightCount from "./actionCreators/Weights/changeweightCount";
import changeTodoWeights from "./actionCreators/Weights/changeTodoWeights";
import changeTotalreps from "./actionCreators/Weights/changeTotalreps";
import changeCompletedSets from "./actionCreators/Swimming/changeCompletedSets.js";
import changeTotaldistance from "./actionCreators/Swimming/changeTotaldistance";
import changetotalSetCount from "./actionCreators/changetotalSetCount.js";
import { useSelector, useDispatch } from "react-redux";
import { useEffect } from "react";
const SWIMMINGLABELS = {
  first: "Stroke",
  second: "Drill",
  third: "Distance",
  fourth: "Repeat",
  total: "Distance",
  unit: "meters",
};
const WEIGHTSLABELS = {
  first: "Movement",
  second: "Weight",
  third: "Reps",
  fourth: "Sets",
  total: "Reps",
  unit: "Reps",
};

//has section for making local storage for counts and objects that need it.
//uses the workouttype chosen from ChooseWorkoutType to move into the correct component.
const CreateSets = () => {
  const workouttype = useSelector((state) => state.workouttype);
  const completedSets = useSelector((state) => state.completedSets);
  const todoSwim = useSelector((state) => state.todoSwim);
  const totaldistance = useSelector((state) => state.totaldistance);
  const swimCount = useSelector((state) => state.swimCount);
  const completedWeight = useSelector((state) => state.completedWeight);
  const todoWeights = useSelector((state) => state.todoWeights);
  const totalreps = useSelector((state) => state.totalreps);
  const weightCount = useSelector((state) => state.weightCount);
  const totalSetCount = useSelector((state) => state.totalSetCount);
  const dispatch = useDispatch();

  useEffect(() => {
    getLocalTodos();
  }, []);

  useEffect(() => {
    saveLocalTodos();
  }, [todoWeights, totalreps, todoSwim, totaldistance, completedSets]);

  const saveLocalTodos = () => {
    localStorage.setItem("todoWeights", JSON.stringify(todoWeights));
    localStorage.setItem("totalreps", JSON.stringify(totalreps));
    localStorage.setItem("completedWeight", JSON.stringify(completedWeight));

    localStorage.setItem("todoSwim", JSON.stringify(todoSwim));
    localStorage.setItem("totaldistance", JSON.stringify(totaldistance));
    localStorage.setItem("completedSets", JSON.stringify(completedSets));
  };

  const getLocalTodos = () => {
    if (localStorage.getItem("todoWeights") === null) {
      localStorage.setItem("todoWeights", JSON.stringify([]));
    } else {
      let todoLocal = JSON.parse(localStorage.getItem("todoWeights"));
      dispatch(changeTodoWeights(todoLocal));
    }

    if (localStorage.getItem("totalreps") === 0) {
      localStorage.setItem("totalreps", JSON.stringify(0));
    } else {
      let totalrepsLocal = JSON.parse(localStorage.getItem("totalreps"));
      dispatch(changeTotalreps(totalrepsLocal));
    }

    if (localStorage.getItem("todoSwim") === null) {
      localStorage.setItem("todoSwim", JSON.stringify([]));
    } else {
      let todoLocal = JSON.parse(localStorage.getItem("todoSwim"));
      dispatch(changeTodoSwim(todoLocal));
    }

    if (localStorage.getItem("completedSets") === null) {
      localStorage.setItem("completedSets", JSON.stringify([]));
    } else {
      let completedSwimLocal = JSON.parse(
        localStorage.getItem("completedSets")
      );
      dispatch(changeCompletedSets(completedSwimLocal));
    }

    if (localStorage.getItem("totaldistance") === 0) {
      localStorage.setItem("totaldistance", JSON.stringify(0));
    } else {
      let totaldistanceLocal = JSON.parse(
        localStorage.getItem("totaldistance")
      );

      dispatch(changeTotaldistance(totaldistanceLocal));
    }
  };

  useEffect(() => {
    getLocalCount();
  }, []);

  useEffect(() => {
    saveLocalCount();
  }, [weightCount, swimCount, totalSetCount]);

  const saveLocalCount = () => {
    localStorage.setItem("weightCount", JSON.stringify(weightCount));
    localStorage.setItem("swimCount", JSON.stringify(swimCount));
    localStorage.setItem("totalSetCount", JSON.stringify(totalSetCount));
  };

  const getLocalCount = () => {
    if (localStorage.getItem("weightCount") === null) {
      localStorage.setItem("weightCount", JSON.stringify(0));
    } else {
      let weightCountLocal = JSON.parse(localStorage.getItem("weightCount"));
      dispatch(changeweightCount(weightCountLocal));
    }

    if (localStorage.getItem("swimCount") === null) {
      localStorage.setItem("swimCount", JSON.stringify(0));
    } else {
      let swimCountLocal = JSON.parse(localStorage.getItem("swimCount"));
      dispatch(changeswimCount(swimCountLocal));
    }

    if (localStorage.getItem("totalSetCount") === null) {
      localStorage.setItem("totalSetCount", JSON.stringify(0));
    } else {
      let totalSetCountLocal = JSON.parse(
        localStorage.getItem("totalSetCount")
      );
      dispatch(changetotalSetCount(totalSetCountLocal));
    }
  };

  switch (workouttype) {
    case "Swimming":
      return (
        <div>
          <SwimmingOptions
            workouttype={workouttype}
            SWIMMINGLABELS={SWIMMINGLABELS}
            todoSwim={todoSwim}
            totaldistance={totaldistance}
            swimCount={swimCount}
            changeTodoSwim={changeTodoSwim}
            changeTotaldistance={changeTotaldistance}
            totalSetCount={totalSetCount}
            completedSets={completedSets}
            changeCompletedSets={changeCompletedSets}
          />
        </div>
      );

    case "Weights":
      return (
        <div>
          <WeightsOptions
            workouttype={workouttype}
            WEIGHTSLABELS={WEIGHTSLABELS}
            todoWeights={todoWeights}
            totalreps={totalreps}
            weightCount={weightCount}
            changeTodoWeights={changeTodoWeights}
            changeTotalreps={changeTotalreps}
            totalSetCount={totalSetCount}
            completedSets={completedSets}
            changeCompletedSets={changeCompletedSets}
          />
        </div>
      );
    case "Completed":
      return (
        <div>
          <CompletedSetsOptions
            completedSets={completedSets}
            SWIMMINGLABELS={SWIMMINGLABELS}
            WEIGHTSLABELS={WEIGHTSLABELS}
          />
        </div>
      );

    default:
      return <h2>Please Select a Workout Type</h2>;
  }
};

export default CreateSets;
