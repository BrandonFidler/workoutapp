export default function changeCompletedSets(completedSets) {
  return { type: "CHANGE_COMPLETEDSETS", payload: completedSets };
}
