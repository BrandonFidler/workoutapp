export default function changeRepeat(repeat) {
  return { type: "CHANGE_REPEAT", payload: repeat };
}
