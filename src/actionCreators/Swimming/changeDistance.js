export default function changeDistance(distance) {
  return { type: "CHANGE_DISTANCE", payload: distance };
}
