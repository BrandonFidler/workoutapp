export default function incrementswimCount(swimCount) {
  return { type: "INCREMENT_SWIMCOUNT", payload: ++swimCount };
}
