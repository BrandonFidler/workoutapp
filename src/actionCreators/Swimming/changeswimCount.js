export default function changeswimCount(swimCount) {
  return { type: "CHANGE_SWIMCOUNT", payload: swimCount };
}
