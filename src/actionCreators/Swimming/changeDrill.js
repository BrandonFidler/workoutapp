export default function changeDrill(drill) {
  return { type: "CHANGE_DRILL", payload: drill };
}
