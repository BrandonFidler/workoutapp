export default function changeStroke(stroke) {
  return { type: "CHANGE_STROKE", payload: stroke };
}
