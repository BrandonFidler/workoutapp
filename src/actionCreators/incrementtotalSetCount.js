export default function incrementtotalSetCount(totalSetCount) {
  return { type: "INCREMENT_TOTALSETCOUNT", payload: ++totalSetCount };
}
