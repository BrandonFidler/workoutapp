export default function changeReps(reps) {
  return { type: "CHANGE_REPS", payload: reps };
}
