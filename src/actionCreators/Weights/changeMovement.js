export default function changeMovement(movement) {
  return { type: "CHANGE_MOVEMENT", payload: movement };
}
