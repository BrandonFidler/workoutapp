export default function changeWeight(weight) {
  return { type: "CHANGE_WEIGHT", payload: weight };
}
