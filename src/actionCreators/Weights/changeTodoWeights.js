export default function changeTodoWeights(todoWeights) {
  return { type: "CHANGE_TODOWEIGHTS", payload: todoWeights };
}
