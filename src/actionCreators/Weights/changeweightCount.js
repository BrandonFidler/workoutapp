export default function changeweightCount(weightCount) {
  return { type: "CHANGE_WEIGHTCOUNT", payload: weightCount };
}
