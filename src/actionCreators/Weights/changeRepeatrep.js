export default function changeRepeatrep(repeatrep) {
  return { type: "CHANGE_REPEATREP", payload: repeatrep };
}
