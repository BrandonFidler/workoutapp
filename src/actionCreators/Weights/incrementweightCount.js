export default function incrementweightCount(weightCount) {
  return { type: "INCREMENT_WEIGHTCOUNT", payload: ++weightCount };
}
