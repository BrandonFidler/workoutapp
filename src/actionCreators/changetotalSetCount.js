export default function changetotalSetCount(totalSetCount) {
  return { type: "CHANGE_TOTALSETCOUNT", payload: totalSetCount };
}
