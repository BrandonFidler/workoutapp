export default function changeWorkouttype(workouttype) {
  return { type: "CHANGE_WORKOUTTYPE", payload: workouttype };
}
