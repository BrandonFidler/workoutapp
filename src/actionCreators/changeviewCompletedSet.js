export default function changeviewCompletedSet(viewCompletedSet) {
  return { type: "CHANGE_VIEWCOMPLETEDSET", payload: viewCompletedSet };
}
