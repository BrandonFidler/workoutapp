import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import store from "./store";
import ChooseWorkoutType from "./ChooseWorkoutType.jsx";
import CreateSets from "./CreateSets.jsx";

//Initial Component which holds Provider, has component ChooseWorkoutType, and CreateSets where most of the work gets done.
//location of header
function App() {
  return (
    <Provider store={store}>
      <div className="p-0 m-0">
        <header className="w-full mb-10 text-center p-7">
          <h1 className="text-6xl text-white"> Brandons Workout App </h1>
        </header>
        <ChooseWorkoutType />
        <CreateSets />
      </div>
    </Provider>
  );
}

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
