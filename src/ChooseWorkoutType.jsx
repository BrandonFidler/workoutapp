import React from "react";
import { useDispatch } from "react-redux";
import changeWorkouttype from "./actionCreators/changeWorkouttype";

//has buttons to choose which section of the app we want to be in:Swimming, Weights, and Completed Sets.
const ChooseWorkoutType = () => {
  const dispatch = useDispatch();

  return (
    <div className="my-0 mx-auto w-7/12">
      <form className="p-8 mb-8 bg-gray-200 shadow-lg rounded-lg flex flex-col md:flex-row  justify-center items-center">
        <div className="form-check">
          <label htmlFor="Swimming">
            <button
              className="selectionButton"
              onClick={(e) => {
                e.preventDefault();
                dispatch(changeWorkouttype("Swimming"));
              }}
            >
              Swimming
            </button>
          </label>
        </div>
        <div className="form-check">
          <label htmlFor="Weights">
            <button
              className="selectionButton"
              onClick={(e) => {
                e.preventDefault();
                dispatch(changeWorkouttype("Weights"));
              }}
            >
              Weights
            </button>
          </label>
        </div>
        <div className="form-check">
          <label htmlFor="Completed">
            <button
              className="selectionButton"
              onClick={(e) => {
                e.preventDefault();
                dispatch(changeWorkouttype("Completed"));
              }}
            >
              Completed Sets
            </button>
          </label>
        </div>
      </form>
    </div>
  );
};

export default ChooseWorkoutType;
