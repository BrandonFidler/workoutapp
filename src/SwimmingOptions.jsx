import React from "react";
import Setlist from "./Setlist.jsx";
import { useSelector, useDispatch } from "react-redux";
import changeStroke from "./actionCreators/Swimming/changeStroke";
import changeRepeat from "./actionCreators/Swimming/changeRepeat";
import changeDrill from "./actionCreators/Swimming/changeDrill";
import changeDistance from "./actionCreators/Swimming/changeDistance";

const STROKES = [
  "Butterfly",
  "Backstroke",
  "Breaststroke",
  "Freestyle",
  "Mixed",
  "IM",
];

//component for the select and input boxes for making the sets for swimming section.
//moves into component Setlist
const SwimmingOptions = ({
  workouttype,
  SWIMMINGLABELS,
  todoSwim,
  totaldistance,
  swimCount,
  changeTodoSwim,
  changeTotaldistance,
  totalSetCount,
  completedSets,
  changeCompletedSets,
}) => {
  const distance = useSelector((state) => state.distance);
  const stroke = useSelector((state) => state.stroke);
  const drill = useSelector((state) => state.drill);
  const repeat = useSelector((state) => state.repeat);
  const dispatch = useDispatch();

  const inputDistanceHandler = (e) => {
    dispatch(changeDistance(+e.target.value));
  };

  const inputRepeatHandler = (e) => {
    dispatch(changeRepeat(+e.target.value));
  };

  const inputDrillHandler = (e) => {
    dispatch(changeDrill(e.target.value));
  };

  const addSetToList = (e) => {
    e.preventDefault();
    if (drill === "") {
      dispatch(
        changeTodoSwim([
          ...todoSwim,
          {
            stroke: stroke,
            drill: "-",
            distance: distance,
            key2: Math.random() * 1000,
            repeat: repeat,
            totaldistance: distance * repeat + totaldistance,
            id: Math.random() * 1000,
          },
        ])
      );
    } else {
      dispatch(
        changeTodoSwim([
          ...todoSwim,
          {
            stroke: stroke,
            drill: drill,
            distance: distance,
            key2: Math.random() * 1000,
            repeat: repeat,
            totaldistance: distance * repeat + totaldistance,
            id: Math.random() * 1000,
          },
        ])
      );
    }
    dispatch(changeTotaldistance(distance * repeat + totaldistance));
  };

  const addRestToList = (e) => {
    e.preventDefault();
    dispatch(
      changeTodoSwim([
        ...todoSwim,
        {
          stroke: "REST",
          drill: drill,
          distance: "",
          key2: Math.random() * 1000,
          repeat: repeat,
          totaldistance: distance + totaldistance,
          rest: true,
          id: Math.random() * 1000,
        },
      ])
    );
  };

  return (
    <div className="my-0 mx-auto w-8/12  2xl:w-11/12">
      <form
        onSubmit={(e) => {
          e.preventDefault();
        }}
        className="  p-10 mb-10 rounded-lg bg-gray-200 shadow-lg flex flex-col 2xl:flex-row  justify-center items-center "
      >
        <label
          htmlFor="stroke"
          className="flex flex-col 2xl:flex-row my-2 lg:mx-10 flex-wrap"
        >
          <i className="font-bold p-2 text-center">Stroke</i>
          <select
            name="stroke"
            id="stroke"
            onChange={(e) => dispatch(changeStroke(e.target.value))}
            onBlur={(e) => dispatch(changeStroke(e.target.value))}
            className="w-52 text-center"
          >
            {STROKES.map((stroke) => (
              <option key={stroke} value={stroke} className="text-center">
                {stroke}
              </option>
            ))}
          </select>
        </label>
        <label
          htmlFor="drill"
          className="flex flex-col 2xl:flex-row mx-10 flex-wrap justify-center items-center"
        >
          <i className="font-bold p-2 text-center">Drill </i>
          <input
            type="text"
            onChange={inputDrillHandler}
            onBlur={inputDrillHandler}
            value={drill}
            className="w-52 text-center"
          />
        </label>
        <label
          htmlFor="distance"
          className="flex flex-col 2xl:flex-row mx-5 flex-wrap justify center items-center"
        >
          <i className="font-bold p-2 text-center">
            Distance (25m Increments){" "}
          </i>
          <input
            type="number"
            step="25"
            min="25"
            onChange={inputDistanceHandler}
            value={distance}
            className="w-52 text-center"
          />
        </label>
        <label
          htmlFor="repeat"
          className="flex flex-col 2xl:flex-row flex-wrap"
        >
          <i className="font-bold p-2 text-center">Repeat</i>
          <input
            type="number"
            step="1"
            min="1"
            value={repeat}
            onChange={inputRepeatHandler}
            onBlur={inputRepeatHandler}
            className="w-52 text-center"
          />
        </label>

        <button
          onClick={addSetToList}
          className="rounded px-6 py-2 color text-white hover:opacity-50 border-none bg-green-500 my-2 md:mx-2 "
        >
          Add Set
        </button>
        <button
          onClick={addRestToList}
          className="rounded px-6 py-2 color text-white hover:opacity-50 border-none bg-blue-500 my-2 md:mx-2 "
        >
          Add Rest
        </button>
      </form>

      <Setlist
        todo={todoSwim}
        changeTodo={changeTodoSwim}
        total={totaldistance}
        changeTotal={changeTotaldistance}
        workouttype={workouttype}
        LABEL={SWIMMINGLABELS}
        swimCount={swimCount}
        totalSetCount={totalSetCount}
        completedSets={completedSets}
        changeCompletedSets={changeCompletedSets}
      />
    </div>
  );
};

export default SwimmingOptions;
