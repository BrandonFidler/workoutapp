import React from "react";
import Setlist from "./Setlist.jsx";
import { useSelector, useDispatch } from "react-redux";
import changeMovement from "./actionCreators/Weights/changeMovement";
import changeRepeatrep from "./actionCreators/Weights/changeRepeatrep";
import changeWeight from "./actionCreators/Weights/changeWeight";
import changeReps from "./actionCreators/Weights/changeReps";
const MOVEMENTS = [
  "Squats",
  "BB Bench",
  "Power Clean",
  "Crunches",
  "SitUps",
  "Rows",
  "Push Ups",
  "Lunges",
  "Burpees",
  "DB Bench",
];

//component for the select and input boxes for making the sets for Weights section.
//moves into component Setlist
const WeightsOptions = ({
  workouttype,
  WEIGHTSLABELS,
  todoWeights,
  totalreps,
  weightCount,
  changeTodoWeights,
  changeTotalreps,
  totalSetCount,
  completedSets,
  changeCompletedSets,
}) => {
  const reps = useSelector((state) => state.reps);
  const movement = useSelector((state) => state.movement);
  const repeatRep = useSelector((state) => state.repeatRep);
  const weight = useSelector((state) => state.weight);
  const dispatch = useDispatch();

  const inputrepsHandler = (e) => {
    dispatch(changeReps(Math.abs(+e.target.value)));
  };

  const inputweightHandler = (e) => {
    dispatch(changeWeight(Math.abs(+e.target.value)));
  };

  const inputrepeatRepHandler = (e) => {
    dispatch(changeRepeatrep(Math.abs(+e.target.value)));
  };

  const submitTodoHandler = (e) => {
    e.preventDefault();
    if (weight === 0) {
      dispatch(
        changeTodoWeights([
          ...todoWeights,
          {
            movement: movement,
            weight: "body",
            reps: reps,
            repeatRep: repeatRep,
            key2: Math.random() * 1000,
            totalreps: reps * repeatRep + totalreps,
            rest: false,
            id: Math.random() * 1000,
          },
        ])
      );
    } else {
      dispatch(
        changeTodoWeights([
          ...todoWeights,
          {
            movement: movement,
            weight: weight,
            reps: reps,
            repeatRep: repeatRep,
            key2: Math.random() * 1000,
            totalreps: reps * repeatRep + totalreps,
            rest: false,
            id: Math.random() * 1000,
          },
        ])
      );
    }
    dispatch(changeTotalreps(reps * repeatRep + totalreps));
  };

  const submitRestHandler = (e) => {
    e.preventDefault();
    dispatch(
      changeTodoWeights([
        ...todoWeights,
        {
          movement: "REST",
          weight: "",
          reps: "",
          key2: Math.random() * 1000,
          repeatRep: repeatRep,
          totalreps: reps * repeatRep + totalreps,
          rest: true,
          id: Math.random() * 1000,
        },
      ])
    );
  };

  return (
    <div className="my-0 mx-auto w-8/12  2xl:w-11/12">
      <form
        onSubmit={(e) => {
          e.preventDefault();
        }}
        className="  p-10 mb-10 rounded-lg bg-gray-200 shadow-lg flex flex-col 2xl:flex-row  justify-center items-center "
      >
        <label
          htmlFor="movement"
          className="flex flex-col 2xl:flex-row my-2 lg:mx-10 flex-wrap items-center"
        >
          <i className="font-bold p-2 text-center">Movement</i>
          <select
            name="movement"
            id="movement"
            onChange={(e) => dispatch(changeMovement(e.target.value))}
            onBlur={(e) => dispatch(changeMovement(e.target.value))}
            className="w-52 text-center"
          >
            {MOVEMENTS.map((movement) => (
              <option key={movement} value={movement} className="text-center">
                {movement}
              </option>
            ))}
          </select>
        </label>
        <label
          htmlFor="weight"
          className="flex flex-col 2xl:flex-row mx-5 flex-wrap items-center"
        >
          <i className="font-bold p-2 text-center">Weight</i>
          <input
            type="number"
            step="2.5"
            onChange={inputweightHandler}
            value={Math.abs(weight)}
            className="w-52 text-center"
          />
        </label>
        <label
          htmlFor="reps"
          className="flex flex-col 2xl:flex-row mx-5 flex-wrap  items-center"
        >
          <i className="font-bold p-2 text-center">Reps</i>
          <input
            type="number"
            step="1"
            onChange={inputrepsHandler}
            value={Math.abs(reps)}
            className="w-52 text-center"
          />
        </label>
        <label
          htmlFor="repeatRep"
          className="flex flex-col 2xl:flex-row flex-wrap"
        >
          <i className="font-bold p-2 text-center">Sets</i>
          <input
            type="number"
            step="1"
            value={repeatRep}
            onChange={inputrepeatRepHandler}
            onBlur={inputrepeatRepHandler}
            className="w-52 text-center"
          />
        </label>

        <button
          onClick={submitTodoHandler}
          className="rounded px-6 py-2 color text-white hover:opacity-50 border-none bg-green-500 my-2 md:mx-2 "
        >
          Add Set
        </button>
        <button
          onClick={submitRestHandler}
          className="rounded px-6 py-2 color text-white hover:opacity-50 border-none bg-blue-500 my-2 md:mx-2 "
        >
          Add Rest
        </button>
      </form>

      <Setlist
        todo={todoWeights}
        changeTodo={changeTodoWeights}
        total={totalreps}
        changeTotal={changeTotalreps}
        workouttype={workouttype}
        LABEL={WEIGHTSLABELS}
        weightCount={weightCount}
        totalSetCount={totalSetCount}
        completedSets={completedSets}
        changeCompletedSets={changeCompletedSets}
      />
    </div>
  );
};

export default WeightsOptions;
