import React from "react";

//Creates the individual item for the chosen set for either Weights or Swimming.
const CompletedSet = ({ todo, todoitem }) => {
  switch (todo.workouttype) {
    case "Weights":
      if (todoitem.stroke === "REST") {
        return (
          <li className="border-t border-black flex flex-row justify-center items-center hover:bg-gray-100">
            <span className="rounded text-lg  p-2 w-28 text-center"> - </span>
            <span className="rounded text-lg p-2 w-28 text-center ">Rest</span>
            <span className="rounded text-lg  p-2 w-28 text-center"> - </span>
            <span className="rounded text-lg  p-2 w-28 text-center"> - </span>
          </li>
        );
      } else {
        return (
          <li className="border-t border-black flex items-center flex-row hover:bg-gray-100 ">
            <span className="rounded text-lg  p-2 w-28 text-center">
              {todoitem.movement}
            </span>
            <span className="rounded text-lg  p-2 w-28 text-center">
              {todoitem.weight}
            </span>
            <span className="rounded text-lg  p-2 w-28 text-center">
              {todoitem.reps}
            </span>

            <span className="rounded text-lg  p-2 w-28 text-center">
              {todoitem.repeatRep}x
            </span>
          </li>
        );
      }

    case "Swimming": {
      if (todoitem.stroke === "REST") {
        return (
          <li className="border-t border-black flex flex-row justify-center items-center hover:bg-gray-100">
            <span className="rounded text-lg  p-2 w-28 text-center"> - </span>
            <span className="rounded text-lg p-2 w-28 text-center ">Rest</span>
            <span className="rounded text-lg  p-2 w-28 text-center"> - </span>
            <span className="rounded text-lg  p-2 w-28 text-center"> - </span>
          </li>
        );
      } else {
        return (
          <li className="border-t border-black flex items-center flex-row hover:bg-gray-100 ">
            <span className="rounded text-lg  p-2 w-28 text-center">
              {todoitem.stroke}
            </span>
            <span className="rounded text-lg  p-2 w-28 text-center">
              {todoitem.drill}
            </span>
            <span className="rounded text-lg  p-2 w-28 text-center">
              {todoitem.distance}m
            </span>

            <span className="rounded text-lg  p-2 w-28 text-center">
              {todoitem.repeat}x
            </span>
          </li>
        );
      }
    }
    default:
      return <div></div>;
  }
};

export default CompletedSet;
