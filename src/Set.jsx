import React from "react";
import { useDispatch } from "react-redux";

//outputs the individual Setlist item form Swimming or Weights
const Set = ({
  todo,
  changeTodo,
  total,
  changeTotal,
  todoitem,
  workouttype,
}) => {
  const dispatch = useDispatch();

  const deleteHandler = () => {
    dispatch(changeTodo(todo.filter((el) => el.id !== todoitem.id)));
    switch (workouttype) {
      case "Swimming":
        dispatch(changeTotal(total - todoitem.distance * todoitem.repeat));
        break;
      case "Weights":
        dispatch(changeTotal(total - todoitem.reps * todoitem.repeatRep));
        break;
      default:
        break;
    }
  };

  if (todoitem.rest) {
    return (
      <li className="border-t border-black flex flex-row items-center hover:bg-gray-100">
        <span className="rounded text-lg  p-2 w-28 text-center"> - </span>
        <span className="rounded text-lg p-2 w-28 text-center ">Rest</span>
        <span className="rounded text-lg  p-2 w-28 text-center"> - </span>
        <span className="rounded text-lg  p-2 w-28 text-center"> - </span>

        <button
          onClick={deleteHandler}
          className=" rounded px-6 py-2 color text-white hover:opacity-50 border-none bg-red-500 mx-2 flex justify-end items-end"
        >
          <i>Delete</i>
        </button>
      </li>
    );
  }
  switch (workouttype) {
    case "Weights":
      return (
        <li className="border-t border-black flex items-center flex-row hover:bg-gray-100 ">
          <span className="rounded text-lg  p-2 w-28 text-center">
            {todoitem.movement}
          </span>
          <span className="rounded text-lg  p-2 w-28 text-center">
            {todoitem.weight}
          </span>
          <span className="rounded text-lg  p-2 w-28 text-center">
            {todoitem.reps}
          </span>

          <span className="rounded text-lg  p-2 w-28 text-center">
            {todoitem.repeatRep}x
          </span>

          <button
            onClick={deleteHandler}
            className=" rounded px-6 py-2 color text-white hover:opacity-50 border-none bg-red-500 mx-2 "
          >
            <i>Delete</i>
          </button>
        </li>
      );

    case "Swimming": {
      return (
        <li className="border-t border-black flex items-center flex-row hover:bg-gray-100 ">
          <span className="rounded text-lg  p-2 w-28 text-center">
            {todoitem.stroke}
          </span>
          <span className="rounded text-lg  p-2 w-28 text-center">
            {todoitem.drill}
          </span>
          <span className="rounded text-lg  p-2 w-28 text-center">
            {todoitem.distance}m
          </span>

          <span className="rounded text-lg  p-2 w-28 text-center">
            {todoitem.repeat}x
          </span>

          <button
            onClick={deleteHandler}
            className=" deleteButton flex items-center"
          >
            <i>Delete</i>
          </button>
        </li>
      );
    }
  }
};

export default Set;
