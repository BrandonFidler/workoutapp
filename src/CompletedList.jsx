import React from "react";
import CompletedSet from "./CompletedSet.jsx";

//creates the list from the chosen completed set.
//moves into Completed set
const CompletedList = ({ todo, SWIMMINGLABELS, WEIGHTSLABELS }) => {
  if (todo.completed !== undefined) {
    if (todo.workouttype === "Swimming") {
      return (
        <div className="flex flex-col justify-center items-center">
          <h1 className="font-bold">{todo.optionlabel}</h1>
          <ul className="flex flex-col border border-black">
            <li className="flex items-center flex-row hover:bg-gray-100 ">
              <span className="setlistheader">{SWIMMINGLABELS.first}</span>
              <span className="setlistheader">{SWIMMINGLABELS.second}</span>
              <span className="setlistheader">{SWIMMINGLABELS.third}</span>
              <span className="setlistheader">{SWIMMINGLABELS.fourth}</span>
            </li>

            {todo.completed.map((todoitem) => (
              <CompletedSet
                todoitem={todoitem}
                todo={todo}
                key={todoitem.key2}
              />
            ))}
          </ul>
        </div>
      );
    } else if (todo.workouttype === "Weights") {
      return (
        <div className="flex flex-col justify-center items-center">
          <h1 className="font-bold">{todo.optionlabel}</h1>
          <ul className="flex flex-col border border-black">
            <li className="flex items-center flex-row hover:bg-gray-100 ">
              <span className="setlistheader">{WEIGHTSLABELS.first}</span>
              <span className="setlistheader">{WEIGHTSLABELS.second}</span>
              <span className="setlistheader">{WEIGHTSLABELS.third}</span>
              <span className="setlistheader">{WEIGHTSLABELS.fourth}</span>
            </li>

            {todo.completed.map((todoitem) => (
              <CompletedSet
                todoitem={todoitem}
                todo={todo}
                key={todoitem.key2}
              />
            ))}
          </ul>
        </div>
      );
    }
  } else {
    return (
      <div className="flex justify-center">
        <i className="font-bold p-2 text-center">Select a previous Workout</i>
      </div>
    );
  }
};

export default CompletedList;
