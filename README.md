Author: Brandon Fidler
email: fidler@unlv.nevada.edu

I have created a workout app as my todo list with the section Swimming and Weights as that is what I like to do.

 You can modify the type of set you want to add then add it by pressing "add set" and if you complete the set press "Complete Set". You can access those completed sets in by pressing the button "completed Sets" at the top. These completed sets have a given name determined by the date, activity, amount of the type of workout.  

I have created the react app with a package manager using parcel and to run it I run:

npm run dev

I created my own css for this project and used a plugin called tailwind. This makes it where you can write inline css that is easier to write and perhaps to read. First time using it so was still learning. 

The Redux files are all inside actionCreators and reducer. 

I put a couple sentences above each component function saying what the function does. 

Link to Pictures:

[Weights](images\bqjycQMD.jpg)

[Swimming](images\xgWj3BFi.jpg)

[Completed Sets](images\E1XNj2Mt.jpg)
